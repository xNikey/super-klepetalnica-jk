/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
 
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
 
function divElementEnostavniTekst(sporocilo) {
  
  var novoSporocilo = vnesiSlike(sporocilo);
  var vsebujeSl = (novoSporocilo != sporocilo);

  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jeKrc = sporocilo.indexOf("&#9756;") > -1;
  
  if (jeSmesko || jeKrc || vsebujeSl) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />")
               
               .split("&lt;br/&gt;").join("<br/>");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  sporocilo = vnesiSlike(sporocilo);
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function vnesiSlike(sporocilo) {

  var besede = sporocilo.split(' ');
  
  // Izlušči link na sliko iz vnosnega besedila
  
  var jeSlika = sporocilo.indexOf("http") > -1;
  if (jeSlika){
    for(var i = 0; i < besede.length; i++){
      
      var zacetek = besede[i].substring(0, 5);
      
      var konec = besede[i].substring(besede[i].length - 4, besede[i].length);
      
      var idZacetka = -1;
      if ((zacetek == ".http") || (zacetek == "!http") || (zacetek == "?http") || (zacetek == ",http")){
        idZacetka = 1;
      }
      else if ((zacetek == "https") || (zacetek == "http/") || (zacetek == "http:")){
        idZacetka = 0;
      }
      
      var idKonca = -1;
      if ((konec == "png.") || (konec == "png!") || (konec == "png?") || (konec == "png,") || 
          (konec == "jpg.") || (konec == "jpg!") || (konec == "jpg?") || (konec == "jpg,") || 
          (konec == "gif.") || (konec == "gif!") || (konec == "gif?") || (konec == "gif,")){
        idKonca = 1;
      }
      else if ((konec == ".png") || (konec == ".jpg") || (konec == ".gif")){
        idKonca = 0;
      }
      
      if ((idKonca > -1) && (idZacetka > -1)) {
        var link = besede[i].substring(idZacetka, besede[i].length - idKonca);
        sporocilo += " <br/><img style='width:200px; margin-left: 20px' src='" + link + "' />";
         
      }
    }
  }
  return sporocilo;
}

/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}

var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    
    var nadimki = klepetApp.getNadimki();
    var noviTekst = sporocilo.besedilo;
    
    var novo = noviTekst.split(" se je preimenoval v ")
    if (novo.length > 1) {
      var staro = novo[0];
      var novo = novo[1].substring(0, novo[1].length - 1)
      if (nadimki.hasOwnProperty(staro)) {
        klepetApp.setNadimki(novo, nadimki[staro]);
      }
    }
    
    nadimki = klepetApp.getNadimki();
    
    var keys = Object.keys(nadimki);
    
    for (var i = 0; i < keys.length; i++){
      var key = keys[i];
      noviTekst = sporocilo.besedilo.split(key).join(nadimki[key] + " (" + key + ")");
    }
    
    
    
    var novElement = divElementEnostavniTekst(noviTekst);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    var nadimki = klepetApp.getNadimki();
    
    for (var i=0; i < uporabniki.length; i++) {
      
      if (nadimki.hasOwnProperty(uporabniki[i])){
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(nadimki[uporabniki[i]] + " (" + uporabniki[i] + ")"));
      }
      else{
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
    }
    // Klik na ime uporabnika ga krcne
    $('#seznam-uporabnikov div').click(function() {
      
      var text = $(this).text();
      var textSeznam = text.split('(')
      
      if (textSeznam.length > 1) {
        textEl = textSeznam[textSeznam.length - 1];
        text = textEl.substring(0, textEl.length - 1);
      }
      
      var sporocilo = '/zasebno ' + '"' + text + '" ' + '"&#9756;"';
      var sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
      if (sistemskoSporocilo) {
        $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      }
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
